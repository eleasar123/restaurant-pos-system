/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurantpossystem.Admin;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author 2ndyrGroupA
 */
public class Accounts extends javax.swing.JFrame {

    /**
     * Creates new form Accounts
     */
    public Accounts() {
        initComponents();
        showAccounts();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        accounts = new javax.swing.JTable();
        addAccount = new javax.swing.JButton();
        goToMenu = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        name = new javax.swing.JTextField();
        username = new javax.swing.JTextField();
        position = new javax.swing.JTextField();
        password = new javax.swing.JPasswordField();
        updatedPassword = new javax.swing.JTextField();
        updatedName = new javax.swing.JTextField();
        updatedPosition = new javax.swing.JTextField();
        idForDelete = new javax.swing.JTextField();
        updatedUsername = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        deleteAccount = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        updatedAccountId = new javax.swing.JTextField();
        updateAccount = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        adminName = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        accounts.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Account_Id", "Name", "Username", "Password", "Position", "Created_at"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        accounts.setDragEnabled(true);
        jScrollPane1.setViewportView(accounts);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 140, 805, 275));

        addAccount.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        addAccount.setText("Add New");
        addAccount.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addAccountMouseClicked(evt);
            }
        });
        getContentPane().add(addAccount, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 390, 130, 39));

        goToMenu.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        goToMenu.setText("Go to Menu");
        goToMenu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                goToMenuMouseClicked(evt);
            }
        });
        getContentPane().add(goToMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 140, -1, 38));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Name:");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 470, 56, 33));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Username:");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 250, 89, 37));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Password:");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 290, 89, 29));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Position:");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 340, 79, 30));

        name.setDragEnabled(true);
        getContentPane().add(name, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 200, 150, 33));

        username.setDragEnabled(true);
        getContentPane().add(username, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 250, 150, 37));

        position.setDragEnabled(true);
        getContentPane().add(position, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 340, 150, 28));

        password.setDragEnabled(true);
        getContentPane().add(password, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 300, 150, 32));

        updatedPassword.setDragEnabled(true);
        getContentPane().add(updatedPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 510, 150, 45));

        updatedName.setDragEnabled(true);
        getContentPane().add(updatedName, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 510, 150, 41));

        updatedPosition.setDragEnabled(true);
        getContentPane().add(updatedPosition, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 510, 150, 45));

        idForDelete.setDragEnabled(true);
        getContentPane().add(idForDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 580, 140, 45));

        updatedUsername.setDragEnabled(true);
        getContentPane().add(updatedUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 510, 150, 45));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setText("Name:");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 200, 66, 32));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("UserName");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 470, 84, 33));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setText("Password:");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 470, 100, 33));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("Position:");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 470, 95, 33));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setText("Account_Id:");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 470, 92, 33));

        deleteAccount.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        deleteAccount.setText("Delete Account");
        deleteAccount.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                deleteAccountMouseClicked(evt);
            }
        });
        getContentPane().add(deleteAccount, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 580, 167, 45));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setText("Account_Id:");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 580, 92, 40));

        updatedAccountId.setDragEnabled(true);
        getContentPane().add(updatedAccountId, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 510, 150, 40));

        updateAccount.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        updateAccount.setText("Update Account");
        updateAccount.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                updateAccountMouseClicked(evt);
            }
        });
        getContentPane().add(updateAccount, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 510, -1, 45));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1080, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 630, 1080, 50));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 60, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 560, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1120, 120, 60, 560));

        jPanel3.setBackground(new java.awt.Color(153, 153, 255));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel11.setBackground(new java.awt.Color(255, 51, 51));
        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("X");
        jLabel11.setOpaque(true);
        jLabel11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel11MouseClicked(evt);
            }
        });
        jPanel3.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(1131, 0, 49, 45));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        jLabel12.setText("Cuisina");
        jPanel3.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 32, 184, 78));

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jButton1.setText("Menu ");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });
        jPanel3.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 23, 173, 74));

        jButton2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jButton2.setText("Transactions");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton2MouseClicked(evt);
            }
        });
        jPanel3.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(553, 23, 214, 74));

        jPanel4.setBackground(new java.awt.Color(204, 204, 255));

        adminName.setBackground(new java.awt.Color(204, 255, 255));
        adminName.setFont(new java.awt.Font("Juice ITC", 1, 18)); // NOI18N
        adminName.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        adminName.setOpaque(true);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(adminName, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(adminName, javax.swing.GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(842, 29, -1, 70));

        jButton3.setBackground(new java.awt.Color(255, 51, 51));
        jButton3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton3.setText("Log out");
        jPanel3.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 57, -1, 40));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1180, 120));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void addAccountMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addAccountMouseClicked
        // TODO add your handling code here:
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/kusina", "root", "");
            Statement stmt = (Statement) con.createStatement();
            String accountName=name.getText();
            String accountUsername=username.getText();
            String accountPassword=password.getText();
            String accountPosition=position.getText();
           
            DateFormat df = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
           TimeZone.setDefault(TimeZone.getTimeZone("Asia/Manila"));
            Date created=new Date();
//            System.out.println(menuName);
//             System.out.println(type);
//             System.out.println(menuPrice);
//             System.out.println(menuAvailability);
            String created_at=df.format(created);
//          System.out.println(created_at);
         
            String query = "Insert into accounts(`account_name`, `username`, `password`, `position`, `created_at`) "+ " values('"+accountName+"','"+accountUsername+"','"+accountPassword+"','"+accountPosition+"','"+created_at+"')";      
            stmt.executeUpdate(query);
             // create a jframe
            JFrame frame = new JFrame();

            // show a joptionpane dialog using showMessageDialog
            JOptionPane.showMessageDialog(frame,
                "Inserted successfully.");
            this.setVisible(false);
            new Accounts().setVisible(true);
          
            
        } catch (Exception e) {
            System.out.println(e);
        }          
    }//GEN-LAST:event_addAccountMouseClicked

    private void goToMenuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_goToMenuMouseClicked
        // TODO add your handling code here:
        this.setVisible(false);
        new AdminMenu().setVisible(true);
    }//GEN-LAST:event_goToMenuMouseClicked

    private void deleteAccountMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deleteAccountMouseClicked
        // TODO add your handling code here:
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/kusina", "root", "");
            Statement stmt = (Statement) con.createStatement();
//            String accountName=name.getText();
//            String accountUsername=username.getText();
//            String accountPassword=password.getText();
//            String accountPosition=position.getText();
           
            DateFormat df = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
           TimeZone.setDefault(TimeZone.getTimeZone("Asia/Manila"));
            Date created=new Date();
//            System.out.println(menuName);
//             System.out.println(type);
//             System.out.println(menuPrice);
//             System.out.println(menuAvailability);
            String deleted_at=df.format(created);
//          System.out.println(created_at);
            String query = "update accounts set deleted_at='"+deleted_at+"' where account_id = '"+idForDelete.getText()+"'";
            
            stmt.executeUpdate(query);
             // create a jframe
            JFrame frame = new JFrame();

            // show a joptionpane dialog using showMessageDialog
            JOptionPane.showMessageDialog(frame,
                "Account deleted successfully!");
            this.setVisible(false);
            new Accounts().setVisible(true);
          
            
        } catch (Exception e) {
            System.out.println(e);
        }          
        
        
    }//GEN-LAST:event_deleteAccountMouseClicked

    private void updateAccountMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_updateAccountMouseClicked
        // TODO add your handling code here:
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/kusina", "root", "");
            Statement stmt = (Statement) con.createStatement();

           
            DateFormat df = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
           TimeZone.setDefault(TimeZone.getTimeZone("Asia/Manila"));
            Date created=new Date();

            String updated_at=df.format(created);

            String query ="update accounts set account_name='"+updatedName.getText()+"', username='"+updatedUsername.getText()+"', password='"+updatedPassword.getText()+"', position='"+updatedPosition.getText()+"',updated_at='"+updated_at+"' where account_id='"+updatedAccountId.getText()+"'";
            
            stmt.executeUpdate(query);
             // create a jframe
            JFrame frame = new JFrame();

            // show a joptionpane dialog using showMessageDialog
            JOptionPane.showMessageDialog(frame,
                "Account updated successfully!");
            this.setVisible(false);
            new Accounts().setVisible(true);
          
            
        } catch (Exception e) {
            System.out.println(e);
        }          
        
    }//GEN-LAST:event_updateAccountMouseClicked

    private void jLabel11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel11MouseClicked
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_jLabel11MouseClicked

    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
        // TODO add your handling code here:
        new AdminMenu().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jButton1MouseClicked

    private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseClicked
        // TODO add your handling code here:
        new Transactions().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jButton2MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Accounts.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Accounts.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Accounts.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Accounts.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                if(new AdminLogIn().userName==null){
                    new AdminLogIn().setVisible(true);
                    
                }else{
                    new Accounts().setVisible(true);
                }
            }
        });
    }
    private void showAccounts(){
         try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/kusina", "root", "");
            Statement stmt = (Statement) con.createStatement();
            
            adminName.setText(new AdminLogIn().userName);
            String query = "SELECT accounts.account_id, accounts.account_name, accounts.username,accounts.password, position.position_name, accounts.created_at, accounts.updated_at, accounts.deleted_at from accounts inner join position on accounts.position=position.position_id where deleted_at IS NULL order by position.position_name asc, accounts.account_name asc ";
            ResultSet rs=stmt.executeQuery(query);
            DefaultTableModel tab=(DefaultTableModel)accounts.getModel();
            while(rs.next()){
//                System.out.println("hello");
               tab.addRow(new Object[]{rs.getInt("account_id"),rs.getString("account_name"),rs.getString("username"),rs.getString("password"),rs.getString("position_name"),rs.getDate("created_at"),
                    "edit"});
            }
            System.out.print("shown");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable accounts;
    private javax.swing.JButton addAccount;
    private javax.swing.JLabel adminName;
    private javax.swing.JButton deleteAccount;
    private javax.swing.JButton goToMenu;
    private javax.swing.JTextField idForDelete;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField name;
    private javax.swing.JPasswordField password;
    private javax.swing.JTextField position;
    private javax.swing.JButton updateAccount;
    private javax.swing.JTextField updatedAccountId;
    private javax.swing.JTextField updatedName;
    private javax.swing.JTextField updatedPassword;
    private javax.swing.JTextField updatedPosition;
    private javax.swing.JTextField updatedUsername;
    private javax.swing.JTextField username;
    // End of variables declaration//GEN-END:variables
}
